provider "aws" {
  region     = "eu-north-1"
}

resource "aws_dynamodb_table" "dynamodb_terraform_sdg_landing_page" {
  name           = "DynamoDB-Terraform-SDGLandingPage"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "submissionId"
  range_key      = "createdAt"

  attribute {
    name = "submissionId"
    type = "S"
  }

  attribute {
    name = "createdAt"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  // Global Secondary Index to include "lastName", "createdAt", "email", and "organization"
  global_secondary_index {
    name               = "SubmissionIndex"
    hash_key           = "submissionId"
    range_key          = "createdAt"  // Optional range key
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["firstName", "lastName", "createdAt", "email", "organization"]
  }

  tags = {
    Name        = "dynamodb-table"
    Environment = "Training"
  }
}

output "dynamodb_table_name" {
  value = aws_dynamodb_table.dynamodb_terraform_sdg_landing_page.name
}
