// jest.config.js
// jest.config.js
// export default {
//   setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
//   testMatch: ['<rootDir>/src/**/*.test.js'],
  // other Jest configurations...
// };

// jest.config.js
export default {
    setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
    testMatch: ['<rootDir>/src/**/*.test.js'],
    transform: {
        '^.+\\.jsx?$': 'babel-jest',
      },
    moduleNameMapper: {
        '\\.svg$':'<rootDir>/src/__mocks__/svgMock.js',
        '\\.css$': 'identity-obj-proxy',
        '\\.png$': 'identity-obj-proxy',
    },
    // testEnvironment: "jsdom"

    // other Jest configurations...
  };