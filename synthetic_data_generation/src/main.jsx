import React from 'react';

import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';

// Add the following code snippet to your application before any other JavaScript/TypeScript code!
// For example put the code in your root index.[ts|js] file, right before you initialize your SPA / App.

import { getWebInstrumentations, initializeFaro } from '@grafana/faro-web-sdk';
import { TracingInstrumentation } from '@grafana/faro-web-tracing';

var faro = initializeFaro({
  url: 'https://faro-collector-prod-us-east-0.grafana.net/collect/a7fb7110a522e101188e1ae3d5e0837d',
  app: {
    name: 'Synthetic Data Generation Landing Page',
    version: '1.0.0',
    environment: 'production'
  },
  sessionTracking: {
    enabled: true,
    samplingRate: 0.5
  },
  instrumentations: [
    // Mandatory, overwriting the instrumentations array would cause the default instrumentations to be omitted
    ...getWebInstrumentations(),

    // Initialization of the tracing package.
    // This packages is optional because it increases the bundle size noticeably. Only add it if you want tracing data.
    new TracingInstrumentation(),
  ],
});

// will be captured
console.info('Website Accessed! Recorded');

// push log explicitly
faro.api.pushLog(['Website Accessed!']);

// push a RUM event
// faro.api.pushEvent('click_sign_up_button_main');

// push error manually
// faro.api.pushError(new Error('oh no main'));

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
