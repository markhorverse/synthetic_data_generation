import { people01, people02, people03, facebook, instagram, linkedin, twitter, airbnb, binance, coinbase, dropbox, send, shield, star  } from "../assets";

export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "features",
    title: "Features",
  },
  {
    id: "product",
    title: "Product",
  },
  {
    id: "clients",
    title: "Clients",
  },
];

export const features = [
  {
    id: "feature-1",
    icon: star,
    title: "Benefits",
    content:
      "Seamless synthetic data generation for project acceleration, accessible worldwide",
  },
  {
    id: "feature-2",
    icon: shield,
    title: "100% Secured",
    content:
      "Proactive measures to safeguard your synthetic data endeavors",
  },
  {
    id: "feature-3",
    icon: send,
    title: "Data Transfer Optimization",
    content:
      " Save resources with our advanced synthetic data solutions.",
  },
];

export const feedback = [
  {
    id: "feedback-1",
    content:
      "Data is only a tool. It will guide you to success, but it won't replace your ingenuity as the developer.",
    name: "M Mobeen",
    title: "Founder & Leader",
    // img: people01,
  },
  {
    id: "feedback-2",
    content:
      "Data is a tool—it guides, but your innovation drives.",
    name: "Awais Afzal",
    title: "Founder & Leader",
    // img: people02,
  },
  // {
  //   id: "feedback-3",
  //   content:
  //     "It is usually people in the money business, finance, and international trade that are really rich.",
  //   name: "Kenn Gallagher",
  //   title: "Founder & Leader",
  //   img: people03,
  // },
];

export const stats = [
  {
    id: "stats-1",
    title: "User Active",
    value: "500+",
  },
  {
    id: "stats-2",
    title: "Trusted by Clients",
    value: "1000+",
  },
  {
    id: "stats-3",
    title: "Profit",
    value: "$100k+",
  },
];

export const footerLinks = [
  {
    title: "Useful Links",
    links: [
      {
        name: "Content",
        link: "https://www.hoobank.com/content/",
      },
      {
        name: "How it Works",
        link: "https://www.hoobank.com/how-it-works/",
      },
      {
        name: "Create",
        link: "https://www.hoobank.com/create/",
      },
      {
        name: "Explore",
        link: "https://www.hoobank.com/explore/",
      },
      {
        name: "Terms & Services",
        link: "https://www.hoobank.com/terms-and-services/",
      },
    ],
  },
  {
    title: "Community",
    links: [
      {
        name: "Help Center",
        link: "https://www.hoobank.com/help-center/",
      },
      {
        name: "Partners",
        link: "https://www.hoobank.com/partners/",
      },
      {
        name: "Suggestions",
        link: "https://www.hoobank.com/suggestions/",
      },
      {
        name: "Blog",
        link: "https://www.hoobank.com/blog/",
      },
      {
        name: "Newsletters",
        link: "https://www.hoobank.com/newsletters/",
      },
    ],
  },
  {
    title: "Partner",
    links: [
      {
        name: "Our Partner",
        link: "https://www.hoobank.com/our-partner/",
      },
      {
        name: "Become a Partner",
        link: "https://www.hoobank.com/become-a-partner/",
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/",
  },
  {
    id: "social-media-2",
    icon: facebook,
    link: "https://www.facebook.com/",
  },
  {
    id: "social-media-3",
    icon: twitter,
    link: "https://www.twitter.com/",
  },
  {
    id: "social-media-4",
    icon: linkedin,
    link: "https://www.linkedin.com/",
  },
];

export const clients = [
  // {
  //   id: "client-1",
  //   logo: airbnb,
  // },
  // {
  //   id: "client-2",
  //   logo: binance,
  // },
  // {
  //   id: "client-3",
  //   logo: coinbase,
  // },
  // {
  //   id: "client-4",
  //   logo: dropbox,
  // },
];