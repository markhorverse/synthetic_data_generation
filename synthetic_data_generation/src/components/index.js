import Navbar from "./Navbar";
import Billing from "./Billing";
import CardDeal from "./CardDeal";
import Business from "./Business";
import Clients from "./Clients";
import CTA from "./CTA";
import Stats from "./Stats";
import Footer from "./Footer";
import Testimonials from "./Testimonials";
import Hero from "./Hero";
import FormComponent from "./FormComponent";
import WaitingListCarousel from "./WaitingListCarousel";

export {
  Navbar,
  Billing,
  CardDeal,
  Business,
  Clients,
  CTA,
  Stats,
  FormComponent,
  WaitingListCarousel,
  Footer,
  Testimonials,
  Hero,
};
