// WaitingListCarousel.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { faro } from '@grafana/faro-web-sdk';

const WaitingListCarousel = () => {
  const [waitingList, setWaitingList] = useState([]);

  useEffect(() => {
    // Fetch the waiting list records from the server
    axios.get('https://sdg-fyp-landing-page-middleware-fast-api.mobeenahmed.repl.co/getAllSubmissions')
      .then(response => {
        setWaitingList(response.data);
        faro.api.pushEvent('DatabaseFetch');
        faro.api.pushLog(['Data fetched from database successfully']);
      })
      .catch(error => {
        console.error(error);
        faro.api.pushError(new Error(error));
        // For testing, add some dummy records
        // setWaitingList([
        //   {
        //     submissionId: 'dummy1',
        //     firstName: 'John',
        //     lastName: 'Doe',
        //     email: 'john.doe@example.com',
        //     organization: 'TestOrg1',
        //     createdAt: 1704055696,
        //   },
        //   {
        //     submissionId: 'dummy2',
        //     firstName: 'Jane',
        //     lastName: 'Doe',
        //     email: 'jane.doe@example.com',
        //     organization: 'TestOrg2',
        //     createdAt: 1704056756,
        //   },
        //   {
        //     submissionId: 'dummy2',
        //     firstName: 'Jane',
        //     lastName: 'Doe',
        //     email: 'jane.doe@example.com',
        //     organization: 'TestOrg2',
        //     createdAt: 1704056756,
        //   },
        //   {
        //     submissionId: 'dummy2',
        //     firstName: 'Jane',
        //     lastName: 'Doe',
        //     email: 'jane.doe@example.com',
        //     organization: 'TestOrg2',
        //     createdAt: 1704056756,
        //   },
        // ]);
      });
  }, []);

  const timeAgo = (timestamp) => {
    const seconds = Math.floor((new Date() - timestamp * 1000) / 1000);

    let interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
      return `${interval} years ago`;
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return `${interval} months ago`;
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return `${interval} days ago`;
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return `${interval} hours ago`;
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return `${interval} minutes ago`;
    }
    return `${Math.floor(seconds)} seconds ago`;
  };

  const sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  // Custom styles for the slider
  const sliderStyles = {
    dot: 'bg-white text-white', // Customize dot color using Tailwind CSS color classes
    // prevArrow: 'text-green-500', // Customize left arrow color using Tailwind CSS color classes
    // nextArrow: 'text-red-500', // Customize right arrow color using Tailwind CSS color classes
  };

  return (
    <div className="mt-8">
      <h2 className="text-2xl text-white font-semibold mb-4 text-center">See Who is on the waiting list</h2>
      <Slider {...sliderSettings} {...sliderStyles}>
        {waitingList.map(record => (
          <div key={record.submissionId} className="p-4">
            <div className="bg-black-gradient-2 p-4 rounded-md shadow-md box-shadow">
              <p className="text-lg text-green-400 font-semibold mb-2">{`${record.firstName} ${record.lastName}`}</p>
              <p className="text-white mb-2">@{record.organization}</p>
              <p className="text-gray-600">Joined {timeAgo(record.createdAt)}</p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default WaitingListCarousel;
