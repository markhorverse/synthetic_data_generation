// FormComponent.js

import React, { useState } from 'react';
import axios from 'axios';
// import styles from "../style";
import { faro } from '@grafana/faro-web-sdk';

const FormComponent = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    organization: '',
    // Add other form fields as needed
  });

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    faro.api.pushEvent('FormInputChange_Detected');
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Add logic to submit the form data (e.g., to an API endpoint)
    // You can use AWS SDK or other methods to interact with DynamoDB   
    axios.post("https://sdg-fyp-landing-page-middleware-fast-api.mobeenahmed.repl.co/submitdata",formData).then(response =>{
      console.info('Form Data Submitted:', response.data);
      faro.api.pushEvent('DatabaseSubmit_Create_Form');
      faro.api.pushLog(['Data Sent Beautifully']);
    })
    .catch(error => {
      console.error(error);
      faro.api.pushError(new Error(error));
    });

    console.info('Form Data Submitted:', formData);

  };

  return (
    <div className="max-w-md mx-auto mt-8 p-6 rounded-md shadow-md sm:flex-row flex-col bg-black-gradient-2 rounded-[20px] box-shadow">
      <h2 className="text-2xl text-white font-semibold mb-4 text-center">Join Waiting List</h2>
      <form onSubmit={handleSubmit} className="flex flex-col items-center">
        <div className="mb-4 w-full max-w-md">
          <label htmlFor="firstName" className="block text-sm font-medium text-white text-center">
            First Name
          </label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            value={formData.firstName}
            onChange={handleInputChange}
            className="mt-1 p-2 w-full border border-black rounded-md bg-transparent box-shadow text-white text-center"
            required
          />
        </div>
        <div className="mb-4 w-full max-w-md">
          <label htmlFor="lastName" className="block text-sm font-medium text-white text-center ">
            Last Name
          </label>
          <input
            type="text"
            id="lastName"
            name="lastName"
            value={formData.lastName}
            onChange={handleInputChange}
            className="mt-1 p-2 w-full border border-black rounded-md bg-transparent box-shadow text-white text-center"
            required
          />
        </div>
        <div className="mb-4 w-full max-w-md">
          <label htmlFor="email" className="block text-sm font-medium text-white text-center">
            Email
          </label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleInputChange}
            className="mt-1 p-2 w-full border border-black rounded-md bg-transparent box-shadow text-white text-center"
            required
          />
        </div>
        <div className="mb-4 w-full max-w-md">
          <label htmlFor="organization" className="block text-sm font-medium text-white text-center">
            Organization
          </label>
          <input
            type="text"
            id="organization"
            name="organization"
            value={formData.organization}
            onChange={handleInputChange}
            className="mt-1 p-2 w-full border border-black rounded-md bg-transparent box-shadow text-white text-center"
            required
          />
        </div>
        {/* Add other form fields as needed */}
        <button
          type="submit"
          className="bg-blue-500 text-white p-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-300"
        >
          Join Waiting List
        </button>
      </form>
    </div>
  );
};

export default FormComponent;
